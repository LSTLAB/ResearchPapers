\documentclass[11pt]{article}
\usepackage[left=2.5cm,right=2.5cm,top=3cm,bottom=3cm,a4paper]{geometry}
\usepackage{graphicx}
\usepackage{dhucs}
\usepackage{mathrsfs,amsmath,amssymb,amscd,mathtools}
\usepackage{array}
\usepackage{algorithm2e}
\usepackage{algpseudocode}
\usepackage{hyperref}
\usepackage{indentfirst}
\usepackage{subfig}
\usepackage{adjustbox}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{color}
\usepackage{setspace}
\usepackage{rotating}
\usepackage{filecontents}
\usepackage{authblk}
\usepackage{soul}
\usepackage{float}
\usepackage{caption}
%\usepackage{subcaption}
\usepackage[square,sort,comma,numbers, sort&compress]{natbib}
\onehalfspacing

\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}
\SetKwRepeat{Do}{do}{while}    
\newcommand{\argmin}{\arg\!\min}
\newcommand{\argmax}{\arg\!\max}

\newtheorem{proposition}{Proposition}
\newtheorem{lemma}{Lemma}
\newtheorem{remark}{Remark}

%for cross refer format changing
\captionsetup[subfigure]{subrefformat=simple,labelformat=simple,listofformat=subsimple}
\renewcommand\thesubfigure{(\alph{subfigure})}
\captionsetup[subtable]{subrefformat=simple,labelformat=simple,listofformat=subsimple}
\renewcommand\thesubtable{(\alph{subtable})}



\begin{document}
	\nocite{*}
	\title{Simulation, $G_t/G_t/1/PS$ under service rate control}
	\author{Yongkyu Cho and Young Myoung Ko\thanks{Correspondence to Young Myoung Ko (youngko@postech.ac.kr)}}
	\affil{Department of Industrial and Management Engineering\\Pohang University of Science and Technology\\ E-mail: jyg1124@postech.ac.kr; youngko@postech.ac.kr}
	%\date{May 31, 2017}
	\maketitle
	
	\begin{abstract}
		
		\emph{Keywords:} $G_t/G_t/1/PS$; Queueing simulation
	\end{abstract}
	
	\section{Introduction} \label{sec:intro}
	
	\section{Nonstationary Processor Sharing Queue} \label{sec:system}
	We describe the queueing model of interest in Section \ref{subsec:model}. In Section \ref{subsec:arrival} and \ref{subsec:service}, we explain the procedures to generate the arrival times and service times. We define some notations here for convenience. 
		\begin{itemize}
			\item $A(t)$: the number of arrivals of customers during the interval $(0,t]$
			\item $D(t)$: the number of departures of customers during the interval $(0,t]$
			\item $Q(t)$: the number of customers in system at time $t$
			\item $V(t)$: the number of arrivals of virtual customers during the interval $(0,t]$
			\item $S(t)$: the virtual sojourn time at time $t$
			\item $W(t)$: the virtual waiting time at time $t$
			\item $R(t)$: the total remaining workloads at time $t$
			\item $A_i$: the time when customer $i$ arrives
			\item $B_i$: the time when customer $i$ begins service
			\item $D_i$: the time when customer $i$ departs
			\item $S_i$: the amount of workload that customer $i$ brings
			\item $V_i$: the duration that customer $i$ spends in service
			\item $W_i$: the duration that customer $i$ waits before starting service 
		\end{itemize}
		\subsection{The Model} \label{subsec:model}
		We consider a nonstationary non-Markovian single server queueing system operating under the processor sharing (PS) discipline with infinite sharing limit: $G_t/G_t/1/PS$ queue. Arrivals into the system occur according to a nonstationary non-Poisson (NSNP) process. We have a time-dependent arrival rate function $\lambda(t)$ that is continuous and bounded finitely both below and above. The $\lambda(t)$ can be integrated to obtain a cumulative arrival rate function $\Lambda(t)\equiv\int_0^t\lambda(s)\textrm{d}s$ for $t\ge 0$. 
		
		Unlike a general FCFS queue, the amount of service that a customer receives is determined at the arrival time. Let $S_i$ be the amount of work (say in bytes) that arrival $i$ brings, and $S_1, S_2, \ldots$ are i.i.d. random variables. The service rate function $\mu(t)$ is time-varying and subject to control in accordance with $\lambda(t)$. The $\mu(t)$ can also be integrated to obtain a cumulative service rate function $M(t)\equiv\int_{0}^{t}\mu(s)\textrm{d}s$. The amount of service that a server provides during a time interval $(t_1,t_2]$ is $M(t_2)-M(t_1)\equiv\int_{t_1}^{t_2}\mu(s)\textrm{d}s$ hence deterministic.
		
		The PS policy is a work-conserving service discipline which is commonly used to describe computer systems (especially CPUs). All the customers or jobs in the system evenly share the server or processor at any given time, hence the name \textit{processor sharing}. For example, if the processor provides $c$ bytes per second and there are $n$ jobs, then each job receives $c/n$ bytes per second of processing capacity.
			
		\subsection{Arrival Process} \label{subsec:arrival}
		Let $A$ be an NSNP arrival process that we want to generate. This process is constructed by perfoming a change of time to a stationary renewal process. Let $N(t)$ be a stationary excess renewal process having inter-renewal times $B_i\sim G(\cdot)$ with $E(B_i)=\tau$, $Var(B_i)=\sigma^2$, and $SCV(B_i)=\sigma^2/\tau^2$. Then, 
		\begin{align}
			E\{N(t)\} &= \frac{t}{\tau}, \\
			Var\{N(t)\} &= \frac{\sigma^2 t}{\tau^3}+o(t)
		\end{align}
		
		\begin{remark}
			$E\{N(t)\}=tE(B_i)$ and $Var\{N(t)\} \approx E\{N(t)\}SCV(B_i).$
		\end{remark}
		In particular, we call $N(t)$ by the standard equilibrium renewal process (SERP) when $\tau=1$. 
		If we define $A(\cdot)$ be the composition of an SERP $N(\cdot)$ and the cumulative arrival rate function $\Lambda(\cdot)$, we obtain $A(t)\equiv N(\Lambda(t))$ with $E\left\{A(t)\right\}=\Lambda(t)$ which is the desired result for NSNP. This procedure is also known as the \textit{change of time}. Since we assume that the rate function $\lambda(t)$ is bounded both below and above, $\Lambda(t)$ is continuous and strictly increasing so that it has a well-defined continuous and strictly increasing inverse function $\Lambda^{-1 }(\cdot)$.    
		\begin{remark}
			Given a cumulative arrival rate function $\Lambda$ and the corresponding NSNP $A$, the base renewal process $N(t)$ can be recovered by letting $N=A\circ\Lambda^{-1}$.
		\end{remark}
		We generate the arrival process using the \textit{inversion method} described in Gerhardt and Nelson (2011). Using the inversion method, we obtain the arrival process $A(t)$ with $E\{A(t)\}=\Lambda(t)$ and $Var\{A(t)\}=SCV(B_i)\Lambda(t)$. The inversion method requires to compute the inverse of the cumulative rate function to generate the time of new arrival, which results in computational burden in many cases. Ma and Whitt (2015) recently proposed an approximation method to reduce the computation time of inversion using the periodicity of rate function. We, however, do not use the approximation method since reducing compuational time is out of our interest in this research study.
		\subsection{Service Times} \label{subsec:service}
		Without loss of generality, we assume that $E(S_i)=1$ for all $i$. In other words, average amount of workload that is brought by a customer is 1. With this setting, the service completion time is determained as soon as a customer arrives if the system is of FCFS. The service completion time, however, is not determined upon arrival when the system is of PS. That is because future arrivals affect customers who have been already in the system. 
		
		The service completion time $D_i $ of a customer $i$ is determined by calculating the following:
		\begin{align}
			D_i=\inf{\left\{x>0: \int_{A_i}^{x}\frac{1}{Q(s)}\mu(s)\textrm{d}s\ge S_i\right\}},
		\end{align}
		where the number of customers in the system $Q(t)$ is varying stochastically with respect to the time $t$.
		Based on the above constructions on the arrival and service times, we will describe the procedure for simulating $G_t/G_t/1/PS$ queue in Section \ref{sec:experiments}. 
	\section{The Service-Rate Controls} \label{sec:controls}
	In this section, we describe the three service rate controls that are applied to simulate $G_t/G_t/1/PS$ queue. Those three controls are originated from the algorithm for multi-server staffing problems and considered by Ma and Whitt. The stabilizing performance of the controls for $G_t/G_t/1$ has been already studied through their two sequential works. We will show that whether or not the result is the same for $G_t/G_t/1/PS$ queue in Section \ref{sec:result}. 
		\subsection{The Rate-Matching Control} \label{subsec:rmcontrol}
		By the rate-matching control, we match the service rate $\mu(t)$ up with the arrival rate $\lambda(t)$ such that the ratio $\lambda(t)/\mu(t)$ remains some desired traffic intensity $\rho$ for the entire time. That is, we control the service rate by
		\begin{align}
			\mu(t)\equiv\frac{\lambda(t)}{\rho},\quad t\ge 0.
		\end{align}
		For $G_t/G_t/1$ queue, the rate-matching control is known to be effective to stabilize the queue length but not the virtual waiting time for any type of periodic arrival rate function in terms of frequency.
		
		\subsection{The Square-Root Control} \label{subsec:srcontrol}
		The square-root control directly borrows its idea from the \textit{square-root staffing formula} for the time-varying multi-server queues
		\begin{align}
			s(t)\equiv m(t)+\xi\sqrt{m(t)}
		\end{align}
		where $s(t)$ is the number of server at $t$, $m(t)$ is an appropriate offered load that corresponds to the expected number of busy servers in an associated infinite-server model, and $\xi$ is the quality-of-service (QoS) parameter. The first square-root control is
		\begin{align}
			\mu(t)=\lambda(t)+\xi\sqrt{\lambda(t)},\quad t\ge 0.	
		\end{align}
		\subsection{The PSA-based Square-Root Control} \label{subsec:psacontrol}
		The \textit{PSA-based square-root control} is firstly assuming that the pointwise stationary approximation (PSA) is appropriate.
		\begin{align}
			\mu(t)\equiv\lambda(t)+\frac{\lambda(t)}{2}\left(\sqrt{1+\frac{\zeta}{\lambda(t)}}-1\right),\quad t\ge 0,
		\end{align}
		where $\zeta$ is a positive parameter.
		\subsection{The PSA-based Difference Matching Control} \label{subsec:diffcontrol}
	\section{The Experiments} \label{sec:experiments}
	In this section, we explain about the experimental procedures and settings for simulating $G_t/G_t/1/PS$ queue with the three service rate controls. 
		\subsection{Estimation of Performance Measures} \label{subsec:pm}
		We consider three performance measures for each service rate control: Mean number in queue ($E\{Q(t)\}$), mean virtual sojourn time ($E\{S(t)\}$), and the expected sojourn time tail probability of virtual sojourn time ($E\left[P\{S(t)\ge\delta\}\right]$). Since all the measures are time dependent, we record these regularly with small time interval. For the mean number in queue, we simply count and record the number of customers in system at each recording epoch. However, we cannot determine the virtual sojourn time $S(t)$ at recording epoch $t$ since the sojourn time is affected not only by the customers already in the system but also by the customers who will arrive the system after $t$. The following expression for the departure time or service completion time of virtual customer $k$ explains this property:
		\begin{align}
			D_k=\inf \left\{x>0:   \int_{A_k}^{x}\frac{1}{1+Q(s)}\mu(s)\textrm{d}s > S_k \right\}.
		\end{align}
		 In order to measure the virtual sojourn time, we generate a virtual customer (indexed by $k$) at each regular recording epoch and record the departure time when the virtual customer is leaving the system. Then, we can record $S(t)$ by $D_k-t$. Once having the recorded $S(t)$, we can easily get the estimated  $E\left[P\{S(t)\ge\delta\}\right]$ as well by counting.		
		\subsection{Algorithm for Discrete Event Simulation} \label{subsec:alg}
		In this subsection, we explain how the discrete event simulation works.
			\subsubsection{Generating the arrival process}
			
			
			\subsubsection{Calculating the service completion times}
		
		\subsection{The Design of Experiments} \label{subsec:doe}
		For most of the experimental settings, we adopt Ma and Whitt (2015) in order to compare the result with $G_t/G_t/1$. Table \ref{summary_doe} summarizes the settings. We use sinusoidal arrival rate function $\lambda(t)=\alpha+\beta\sin{(\gamma t)}$ with parametric constants $\alpha=1$, $\beta=0.2$, and $\gamma=0.001,0.01,0.1,1,10$. Therefore, we have five functions of the same amplitude but of different periods. For service rate function $\mu(t)$, we use the three service rate controls that are described in Section \ref{sec:controls}. The control parameter is 0.8 for RM, 0.2 for SR, 1.0 for PSA. For the base distribution of NSNP, we use three different distributions in terms of SCV in order to know the effect of variability. We use Erlang distribution (sum of two i.i.d. exponentials) for low SCV, exponential distribution for middle SCV, and Hyperexponential distribution (mixture of two exponentials) for high SCV. For each independent system, we conduct 10,000 replications of simulation. The length of each replication is 20,000 unit times for $\gamma=0.001$ and 2,000 unit times for the other $\gamma$s. Finally, we record the performance measures in every 10 unit times for $\gamma=0.001$ and 1 unit time for the other $\gamma$s.

		\begin{table}[H]
			\centering
			\caption{Summary of the experimental settings}
			\label{summary_doe}
			\resizebox{\textwidth}{!}
			{
				\begin{tabular}{|c|c|c|c|}
					\hline
					& Rate matching                         & Square-root                                   & PSA-based square-root                                                                                  \\ \hline
					Arrival rate function                      & \multicolumn{3}{c|}{\begin{tabular}[c]{@{}c@{}}$\lambda(t)=\alpha + \beta \sin{(\gamma t)}$\\ $\alpha =1,\beta =0.2,\gamma=0.001,0.01,0.1,1,10$\end{tabular}}                                  \\ \hline
					Service rate function                      & $\mu(t)\equiv\frac{\lambda(t)}{\rho}$ & $\mu(t)\equiv\lambda(t)+\xi\sqrt{\lambda(t)}$ & $\mu(t)\equiv\lambda(t)+\frac{\lambda(t)}{2}\left(\sqrt{\frac{\lambda(t)+\zeta}{\lambda(t)}}-1\right)$ \\ \hline
					Control parameter                          & $\rho=0.8$                            & $\xi=0.2$                                     & $\zeta=1.0$  
					\\ \hline
					\multirow{2}{*}{Arrival base distribution} & \multicolumn{3}{c|}{\multirow{4}{*}{\begin{tabular}[c]{@{}c@{}}Exponential (mean=1.0, SCV=1.0) \\ Hyperexponential (mean=1.0, SCV=4.0)\\ Erlang (mean=1.0, SCV=0.5)\end{tabular}}}               \\
					& \multicolumn{3}{c|}{}                                                                                                                                                                          \\ \cline{1-1}
					\multirow{2}{*}{Workload distribution}     & \multicolumn{3}{c|}{}                                                                                                                                                                          \\
					& \multicolumn{3}{c|}{}                                                                                                                                                                                                                                                                    \\ \hline
					Number of replications                      & \multicolumn{3}{c|}{10,000}                                                                                                                                                                     \\ \hline
					Replication length                         & \multicolumn{3}{c|}{\begin{tabular}[c]{@{}c@{}}20000 unit times for $\gamma=0.001$\\ 2000 unit times for $\gamma=0.01,0.1,1,10$\end{tabular}}                                                                       \\ \hline
					Recording interval                         & \multicolumn{3}{c|}{\begin{tabular}[c]{@{}c@{}}10 unit times for $\gamma=0.001$\\ 1 unit time for $\gamma=0.01,0.1,1,10$\end{tabular}}                                                                                                                                                                    \\ \hline
				\end{tabular}
			}
		\end{table}
	
	\section{Simulation Results} \label{sec:result}
	In this section, we show the results from the simulations of $G_t/G_t/1/PS$ queues. For comparison, we contain the results of $G_t/G_t/1$ queues as well under the same settings other than PS. As explained in Section \ref{subsec:pm}, we analyze the performance of queues in terms of the number in queue ($E\{Q(t)\}$), the virtual sojourn time ($E\{S(t)\}$), and the virtual sojourn time tail probability ($E\left[P\{S(t)\ge\delta\}\right]$).
		\subsection{The Rate-Matching Control}
		We first look at the result from the Markovian queues. Figure \ref{fig:rm_markovian_mean_number_wait} depicts the time-varying means of the number in queue (green line) and the virtual sojourn times (red line) of rate-matching-controlled $G_t/G_t/1$ queues (left: FCFS, right: PS) where base arrival interval time and workload are both exponentially distributed. Hence, figures on the left are $M_t/M_t/1$ and figures on the right are $M_t/M_t/1/PS$. It is known from Whitt that the rate-matching control effectively stabilizes the number of customers in $G_t/G_t/1$ queues but does not stabilize the virtual sojourn times. The result is similar for $M_t/M_t/1/PS$ as can be seen in the figures on the right side. It stabilizes the number in queue for all $\gamma$s but the virtural sojourn time is not stabilized except for $\gamma=10$. When the period of time-varying arrival rate function is too short (i.e., $\gamma$ is too large), the system behaves like having constant arrival rate that equals the average $\bar{\lambda}\equiv\lim_{t\to\infty}\frac{\int_{0}^{t}\lambda(s)\textrm{d}s}{t}$, which is why we see the well-stabilized result from Figure \ref{fig:rm_exp_exp_mean_number_wait_10}. Another remarkable effect of PS policy is that the virtual sojourn times in $M_t/M_t/1/PS$ queues are consistently smaller than those of $M_t/M_t/1$ queues.
		\begin{remark}
			The PS policy does not affect the number of customers in single-server queues with time-varying arrival rate. However, it reduces the virtual sojourn times of the queues.
		\end{remark}
		Figure \ref{fig:rm_markovian_tail_prob} depicts the virtual sojourn time tail probabilities of $M_t/M_t/1$ and $M_t/M_t/1/PS$ queues. We can notice from the figure that the probabilities vary periodically for both policies (FCFS and PS). However, one positive effect on stabilization is that the amplitudes of the graphs are significantly reduced comparing to the arrival rate function. Remarkably, when $\gamma = 10$, 
		\begin{remark}
			The rate-matching control stabilizes the virtural sojourn time tail probability of single-server queues with time-varying arrival rate to some degree. However, periodic fluctuation still remains.
		\end{remark}
		\begin{figure}[H]
			\centering
			\subfloat[][$\gamma=0.001$]
			{
				\centering\includegraphics[width=\linewidth]{fig_RM_EXP_EXP_mean_number_wait_10_-3}
				\label{fig:rm_exp_exp_mean_number_wait_0.001}
			}
		
			\subfloat[][$\gamma=0.1$]
			{
				\centering\includegraphics[width=\linewidth]{fig_RM_EXP_EXP_mean_number_wait_10_-1}
				\label{fig:rm_exp_exp_mean_number_wait_0.1}
			}
		
			\subfloat[][$\gamma=10.0$]
			{
				\centering\includegraphics[width=\linewidth]{fig_RM_EXP_EXP_mean_number_wait_10_1}
				\label{fig:rm_exp_exp_mean_number_wait_10}
			}
			\caption{Comparison of mean number in queue and mean sojourn time between $M_t/M_t/1$ and $M_t/M_t/1/PS$ }
			\label{fig:rm_markovian_mean_number_wait}
		\end{figure}
	
		\begin{figure}[H]
			\centering
			\subfloat[][$\gamma=0.001$]
			{
				\centering\includegraphics[width=\linewidth]{fig_RM_EXP_EXP_tail_prob_10_-3}
				\label{fig:rm_exp_exp_tail_prob_0.001}
			}
			
			\subfloat[][$\gamma=0.1$]
			{
				\centering\includegraphics[width=\linewidth]{fig_RM_EXP_EXP_tail_prob_10_-1}
				\label{fig:rm_exp_exp_tail_prob_0.1}
			}
			
			\subfloat[][$\gamma=10.0$]
			{
				\centering\includegraphics[width=\linewidth]{fig_RM_EXP_EXP_tail_prob_10_1}
				\label{fig:rm_exp_exp_tail_prob_10}
			}
			\caption{Comparison of sojourn time tail probabilities between $M_t/M_t/1$ and $M_t/M_t/1/PS$ }
			\label{fig:rm_markovian_tail_prob}
		\end{figure}
		\subsection{The Square-Root Control}
		
		\subsection{The PSA-Based Square-Root control}
	
	\section{Conclusion} \label{sec:con}
	Unlike the $G_t/G_t/1$ queue, performance measures are 
	
	\section*{Acknowledgment} \label{sec:ack}
	
	
	\bibliographystyle{plainnat}
	\begin{thebibliography}{9} 
		\bibitem{Anselmi2011}
		J. Anselmi and I.M. Verloop, Energy-aware capacity scaling in virtualized environments with performance guarantees, Perform Eval 68 (2011), 1207--1221.
		
		\bibitem{Barroso2009}
		L.A. Barroso and U. H\"{o}lzle, The datacenter as a computer: An introduction to the design of warehouse-scale machines, Morgan and Claypool, San Rafael, CA, USA, 2009.
		
		\bibitem{Chen2005}
		Y. Chen, A. Das, W. Qin, A. Sivasubramaniam, Q. Wang, and N. Gautam, Managing server energy and operational costs in hosting centers, Proc of the 2005 ACM SIGMETRICS, New York, NY, USA, 2005, pp. 303--314.
		
		\bibitem{Chen2009}
		Y. Chen and N. Gautam, Server frequency control using Markov decision processes, Proc of the 28th Conference on Computer Communications (IEEE INFOCOM '09), Rio de Janeiro, Brazil, pp. 2951--2955.
		
		\bibitem{Ko2014}
		Y.M. Ko and Y. Cho, A distributed speed scaling and load balancing algorithm for energy efficient data centers, Perform Eval 79 (2014), 120--133.	
		
		\bibitem{Kwon2016}
		S. Kwon and N. Gautam, Guaranteeing performance based on time-stability for energy-efficient data centers, IIE Trans 48 (2016), 812--825.
		
	\end{thebibliography}
\end{document}


